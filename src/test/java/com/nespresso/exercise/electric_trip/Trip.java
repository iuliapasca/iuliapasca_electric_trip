package com.nespresso.exercise.electric_trip;

/**
 * Model class for a trip between two cities.
 *
 * @author iulia.pasca@gmail.com
 */
public class Trip {

    /** The name of the first city. */
    private String firstCityName;
    /** The name of the second city (the destination). */
    private String secondCityName;
    /** The distance between the two cities. */
    private Integer distance;

    public Trip(String firstCityName, String secondCityName, Integer distance) {
        this.firstCityName = firstCityName;
        this.secondCityName = secondCityName;
        this.distance = distance;
    }

    public String getFirstCityName() {
        return firstCityName;
    }

    public void setFirstCityName(String firstCityName) {
        this.firstCityName = firstCityName;
    }

    public String getSecondCityName() {
        return secondCityName;
    }

    public void setSecondCityName(String secondCityName) {
        this.secondCityName = secondCityName;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
}
