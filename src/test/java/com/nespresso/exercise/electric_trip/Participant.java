package com.nespresso.exercise.electric_trip;

/**
 * Model class for a participant.
 *
 * @author iulia.pasca@gmail.com
 */
public class Participant {

    /** Constant holding the maximum charge. */
    private static final Integer MAX_CHARGE = 100;

    /** The id of the participant. */
    private Integer participantId;
    /** The city from which the participant starts the trip. */
    private String startingCityName;
    /** The battery size. */
    private Integer batterySize;
    /** The low speed performance. */
    private Integer lowSpeedPerformance;
    /** The high speed perfoemance. */
    private Integer highSpeedPerformance;
    /** Flag which indicates if the participant started the trip. */
    private boolean active;
    /** The current city name. */
    private String currentCityName;
    /** The current charge status. */
    private Integer currentCharge;

    public Participant(Integer participantId, String startingCityName, Integer batterySize, Integer lowSpeedPerformance,
                       Integer highSpeedPerformance) {
        this.participantId = participantId;
        this.startingCityName = startingCityName;
        this.currentCityName = startingCityName;
        this.batterySize = batterySize;
        this.lowSpeedPerformance = lowSpeedPerformance;
        this.highSpeedPerformance = highSpeedPerformance;
        this.currentCharge = MAX_CHARGE;
        this.active = false;
    }

    public Integer getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Integer participantId) {
        this.participantId = participantId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getStartingCityName() {
        return startingCityName;
    }

    public void setStartingCityName(String startingCityName) {
        this.startingCityName = startingCityName;
    }

    public Integer getBatterySize() {
        return batterySize;
    }

    public void setBatterySize(Integer batterySize) {
        this.batterySize = batterySize;
    }

    public Integer getLowSpeedPerformance() {
        return lowSpeedPerformance;
    }

    public void setLowSpeedPerformance(Integer lowSpeedPerformance) {
        this.lowSpeedPerformance = lowSpeedPerformance;
    }

    public Integer getHighSpeedPerformance() {
        return highSpeedPerformance;
    }

    public void setHighSpeedPerformance(Integer highSpeedPerformance) {
        this.highSpeedPerformance = highSpeedPerformance;
    }

    public String getCurrentCityName() {
        return currentCityName;
    }

    public void setCurrentCityName(String currentCityName) {
        this.currentCityName = currentCityName;
    }

    public Integer getCurrentCharge() {
        return currentCharge;
    }

    public void setCurrentCharge(Integer currentCharge) {
        this.currentCharge = currentCharge;
    }
}
