package com.nespresso.exercise.electric_trip;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Model class for electric trip.
 *
 * @author iulia.pasca@gmail.com
 */
public class ElectricTrip {

    private static final String SEPARATOR = "-";
    private static final String CHARGE_SEPARATOR = ":";
    private static final String PERCENTAGE_SYMBOL = "%";

    /** List of trips between two cities. */
    private List<Trip> trips;
    /** The participant. */
    private List<Participant> participants;

    public ElectricTrip(String tripDetails) {
        participants = new ArrayList<Participant>();
        trips = new ArrayList<Trip>();

      String[] parts = tripDetails.split(SEPARATOR);
      for (int i = 0; i < parts.length - 2; i++) {
          if (i % 2 == 0) {
              String firstCityName;
              if (parts[i].contains(CHARGE_SEPARATOR)) {
                  String[] cityParts = parts[i].split(CHARGE_SEPARATOR);
                  firstCityName = cityParts[0];
              } else {
                  firstCityName = parts[i];
              }
              String secondCityName;
              if (parts[i + 2].contains(CHARGE_SEPARATOR)) {
                  String[] cityParts = parts[i + 2].split(CHARGE_SEPARATOR);
                  secondCityName = cityParts[0];
              } else {
                  secondCityName = parts[i + 2];
              }
              Integer distance = Integer.valueOf(parts[i + 1]);
              Trip trip = new Trip(firstCityName, secondCityName, distance);
              System.out.println("Trip from [" + firstCityName + "] to [" + secondCityName + "] - " + distance + " km");
              trips.add(trip);
          }
      }
    }

    /**
     * Creates the participant and the starting point of the trip.
     *
     * @param cityName the starting city name
     * @param batterySize the battery size
     * @param lowSpeedPerformance the low speed performance
     * @param highSpeedPerformance the high speed performance
     * @return the participant id
     */
    public int startTripIn(final String cityName, Integer batterySize, Integer lowSpeedPerformance,
                           Integer highSpeedPerformance) {
        int participantId = getParticipantId(cityName);
        Participant participant =
                new Participant(participantId, cityName, batterySize, lowSpeedPerformance, highSpeedPerformance);
        participants.add(participant);

        return participantId;
    }

    private int getParticipantId(String cityName) {
        if (cityName == null) {
            return 0;
        }
        // todo rewrite this check
        // trips.stream().filter(x -> x.getFirstCityName().equals(cityName)).count()
        boolean isInList = false;
        for (Trip trip : trips) {
            if (cityName.equals(trip.getFirstCityName()) || cityName.equals(trip.getSecondCityName())) {
                isInList = true;
                break;
            }
        }
        if (!isInList) {
            return 0;
        }

        Random random = new Random();
        return random.nextInt();
    }

    /**
     * Starts the trip by marking as active the participant with the given id. The participant is moving at low speed.
     *
     * @param participantId the participant id
     */
    public void go(int participantId) {
      move(participantId, MovingType.LOW);
    }

    /**
     * Retrieves the location of the participant with the given id.
     *
     * @param participantId the participant id
     * @return a city name indicating the location of the participant
     */
    public String locationOf(final int participantId) {
        Participant participant = getParticipantById(participantId);
        if (participant == null) {
            return null;
        }
        return participant.getCurrentCityName();
    }

    /**
     * Retrieves the current charge status for the participant with th given id.
     *
     * @param participantId the participant id
     * @return the current charge status
     */
    public String chargeOf(int participantId) {
        Participant participant = getParticipantById(participantId);
        if (participant == null) {
            return null;
        }
        if (participant.getCurrentCharge() == null) {
            return "0" + PERCENTAGE_SYMBOL;
        }
        return participant.getCurrentCharge().toString() + PERCENTAGE_SYMBOL;
    }

    private Participant getParticipantById(int participantId) {
        for (Participant participant : participants) {
            if (participant.getParticipantId() == participantId) {
                return participant;
            }
        }
        return null;
    }

    /**
     * Charges the participant with the given id by amount * the KWh charged per hour available in the current city.
     *
     * @param participantId the participant id
     * @param amount the no of hours to charge
     */
    public void charge(int participantId, int amount) {
       Participant participant = getParticipantById(participantId);
       if (participant == null) {
           return;
       }
       Integer currentCharge = participant.getCurrentCharge();
       // todo get KWh charged per hour of charge time for current city e.g. 25
       int charge = amount * 25;
       if (currentCharge + charge < 100) {
           participant.setCurrentCharge(currentCharge + charge);
       } else {
           participant.setCurrentCharge(100);
       }
    }

    /**
     * Starts the trip by marking as active the participant with the given id. The participant is moving at high speed.
     *
     * @param participantId the participant id
     */
    public void sprint(int participantId) {
      move(participantId, MovingType.HIGH);
    }

    private void move (int participantId, MovingType type) {
        for (Participant participant : participants) {
            if (participant.getParticipantId() == participantId) {
                participant.setActive(true);

                // as long as the participant still has charge, he will go further
                while (participant.getCurrentCharge() != null && participant.getCurrentCharge() > 0) {
                    // get available trips
                    String currentCityName = participant.getCurrentCityName();
                    List<Trip> availableTrips = new ArrayList<Trip>();
                    for (Trip trip : trips) {
                        if (trip.getFirstCityName().equals(currentCityName)) {
                            availableTrips.add(trip);
                        }
                    }
                    if (availableTrips.isEmpty()) {
                        return;
                    }

                    Trip firstTrip = availableTrips.get(0);
                    int distance = firstTrip.getDistance();
                    int speedPerformance = getSpeedPerformance(participant, type);
                    int maxDistance = participant.getBatterySize() * speedPerformance;
                    if (distance > maxDistance) {
                        return;
                    }
                    Integer currentCharge = participant.getCurrentCharge();
                    Integer roundedCharge = Math.round(distance * 100 / maxDistance);
                    if (currentCharge - roundedCharge < 0) {
                        return;
                    }
                    Integer charge = currentCharge - roundedCharge;
                    participant.setCurrentCharge(charge);
                    participant.setCurrentCityName(firstTrip.getSecondCityName());
                    System.out.println("Participant with id [" + participantId + "] is in ["
                            + participant.getCurrentCityName() + "] and has [" + participant.getCurrentCharge()
                            + "] charge.");

                }
            }
        }
    }

    private Integer getSpeedPerformance(Participant participant, MovingType type) {
        switch (type) {
            case LOW:
                return participant.getLowSpeedPerformance();
            case HIGH:
                return participant.getHighSpeedPerformance();
            default:
                return 0;
        }
    }
}
