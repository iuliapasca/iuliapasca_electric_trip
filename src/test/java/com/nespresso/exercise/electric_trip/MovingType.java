package com.nespresso.exercise.electric_trip;

/**
 * Enum for the speed performance.
 *
 * @author iulia.pasca@gmail.com
 */
public enum MovingType {
    /** Low speed performance. */
    LOW,
    /** High speed performance. */
    HIGH;
}
